/**************************
 *Scott Logan
 *CPSC 1021, Section 4, F20
 *salogan@clemson.edu
 *Elliot McMillan
 **************************/
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>

using namespace std;


typedef struct Employee{
  string lastName;
	string firstName;
	int birthYear;
	double hourlyWage;
}employee;

bool name_order(const employee& lhs, const employee& rhs);
int myrandom (int i) { return rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));


  /*Create an array of 10 employees and fill information from standard input with prompt messages*/
  // Takes input to populate the array of employees, with validation loops
  employee employeeList[10];
  for (int i = 0; i < 10; i++) {
    cout << "Please enter last name : ";
    while (!(cin >> employeeList[i].lastName)) {
      cin.clear();
      cin.ignore();
      cout << "Please enter valid last name: ";
    }

    cout << "Please enter first name: ";
    while (!(cin >> employeeList[i].firstName)) {
      cin.clear();
      cin.ignore();
      cout << "Please enter valid first name: ";
    }

    cout << "Please enter birth year: ";
    while (!(cin >> employeeList[i].birthYear)) {
      cin.clear();
      cin.ignore();
      cout << "Please enter valid birth year: ";
    }

    cout << "Please enter hourly wage: ";
    while (!(cin >> employeeList[i].hourlyWage)) {
      cin.clear();
      cin.ignore();
      cout << "Please enter valid hourly wage: ";
    }

  }


  /*After the array is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
  // Shuffles the array
  random_shuffle(&employeeList[0], &employeeList[10], myrandom);

   /*Build a smaller array of 5 employees from the first five cards of the array created
    *above*/
   // Populate the smaller employee array with the first 5 elements of the shuffled array
   employee employee5List[5] = {employeeList[0], employeeList[1], employeeList[2], employeeList[3], employeeList[4]};


    /*Sort the new array.  Links to how to call this function is in the specs
     *provided*/
      // Sort the smaller employee array according to last name order
      sort(&employee5List[0], &employee5List[5], name_order);

    /*Now print the array below */
    // Loop through each element in the Employee List
    for (auto&& i : employee5List) {
      cout << setw(25) << "Name: " << i.lastName << ", " << i.firstName << "\n";
      cout << setw(25) << "Birth Year: " << i.birthYear << "\n";
      // Applies flags to the cout
      cout << setw(25) << fixed << showpoint << setprecision(2) << "Wage: " << i.hourlyWage << "\n\n";
    }



  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool name_order(const employee& lhs, const employee& rhs) {
  if (lhs.lastName < rhs.lastName)
    return true;
  else
    return false;
}

